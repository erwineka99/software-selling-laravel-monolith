<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->Increments('id');
            $table->string('nama',50)->unique();
            $table->timestamps();
        });

        DB::table('user_role')->insert(
            array(
                'nama' => 'super admin',
                'created_at' => now(),
                'updated_at' => now(),
            )
        );
        DB::table('user_role')->insert(
            array(
                'nama' => 'pegawai',
                'created_at' => now(),
                'updated_at' => now(),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role');
    }
}
