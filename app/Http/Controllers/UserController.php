<?php

namespace App\Http\Controllers;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    function index()
    {
        $user=User::all();

        $user=DB::table('users')
        ->select('users.id as id','users.name as name','users.password as password','users.email as email','users.role_id as user_role_id', 'user_role.id as user_role_id','user_role.nama as user_role_nama')
        ->join('user_role','user_role.id','=','users.role_id')
        ->get();
        return view('user.index', compact('user'));
    }
    function create()
    {
        $user_role=UserRole::all();

        return view('user.create',compact('user_role'));
    }
    function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'role_id'=>'required'
        ]);
        $user=User::create([
            'name' => $request["name"],
            'email'=> $request["email"],
            'password'=>Hash::make($request['password']),
            'role_id'=>$request["role_id"]
            ]);
        return redirect('/user');
    }
    function show($id)
    {
        $user = User::find($id);

        return view('user.show', compact('user'));
    }

    function edit($id)
    {
        $user = User::find($id);
        $user_role=UserRole::all();
        return view('user.edit', compact('user','user_role'));
    }

    function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'role_id'=>'required'
        ]);
        User::find($id)->update([
            'name' => $request["name"],
            'email'=> $request["email"],
            'password'=>Hash::make($request['password']),
            'role_id'=>$request["role_id"]
            ]);
        return redirect('/user');
    }

    function destroy($id)
    {
        User::destroy($id);
        return redirect('/user');
    }
}
