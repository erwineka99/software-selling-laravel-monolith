<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Transaksi;
use App\TransaksiDetail;
use DB;

class DashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function count()
    {
        $user_count = DB::table('users')->count();
        //->first();
        //dd($transaksi_count);

        $transaksi_count = DB::table('transaksi')->count();
        //->first();
        //dd($transaksi_count);

        $transaksi_sum = DB::table('transaksi')->sum('jumlah_total');
       // dd($transaksi_sum);

       $item_count = DB::table('item')->count();
        //->first();
        //dd($transaksi_count);

        return view('dashboard',compact('user_count','transaksi_count','transaksi_sum','item_count'));
    }
}
