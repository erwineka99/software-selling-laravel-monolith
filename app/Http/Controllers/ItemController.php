<?php

namespace App\Http\Controllers;
use App\Item;
use App\ItemKategori;
use Illuminate\Http\Request;
use DB;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        //$item=Item::all();

        //$item= Item::with('ItemKategori')->get();
        $item=DB::table('item')
        ->select('item.id as id','item.nama as nama','item.harga as harga','item.item_kategori as item_kategori_id','item_kategori.nama as item_kategori_nama')
        ->join('item_kategori','item_kategori.id','=','item.item_kategori')
        ->get();

        return view("items.index", compact('item'));
    }


    public function create()
    {
        $item_kategori=ItemKategori::all();

        return view('items.create', compact('item_kategori'));
    }

     function store(Request $request)
    {
        //
        $request->validate([
                'nama' => 'required',
                'harga' => 'required',
                'item_kategori' => 'required'
        ]);
        $item=Item::create([
            'nama' => $request["nama"],
            'harga'=>$request["harga"],
            'item_kategori' => $request["item_kategori"]
        ]);
        return redirect('/item');

    }

     function show($id)
    {
        //
        $item = Item::find($id);
        return view('items.show', compact('item'));
    }


    public function edit($id)
    {
        //
        $item = Item::find($id);
        $item_kategori=ItemKategori::all();
        return view('items.edit', compact('item','item_kategori'));
    }


    public function update(Request $request, $id)
    {

         $request->validate([
             'nama' => 'required',
             'harga' => 'required',
             'item_kategori' => 'required'
         ]);

        Item::where('id', $id)
            ->update([
            'nama' => $request["nama"],
            'harga'=>$request["harga"],
            'item_kategori' => $request["item_kategori"]
        ]);

        return redirect('/item');
    }

     function destroy($id)
    {
        //
        Item::destroy($id);
        return redirect('/item');
    }
}
