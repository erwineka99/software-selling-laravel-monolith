<?php

namespace App\Http\Controllers;

use App\UserRole;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        //$pertanyaan = DB::table('pertanyaan')->get();

        //eloquent
        $user_role=UserRole::all();
        return view('user_role.index', compact('user_role'));
    }

    function create(){

        return view('user_role.create');
    }

    function store(Request $request){
        $request->validate([
            'nama' => 'required|unique:user_role',
        ]);

        $user_role=UserRole::create(['nama' => $request["nama"]]);

        return redirect('/userrole');
    }

    function show($id){
        //$pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        $user_role = UserRole::find($id);

        return view('user_role.show', compact('user_role'));
    }

    function edit($id){
        $user_role = UserRole::find($id);
        return view('user_role.edit', compact('user_role'));
    }

    function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
        ]);
        //mass update
        UserRole::where('id', $id)
          ->update(['nama' =>  $request["nama"],
                    ]);


        // $query = DB::table('pertanyaan')
        //     ->where('id',$pertanyaan_id)
        //     ->update([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"],
        //     "tanggal_diperbaharui" => date("Y-m-d")
        // ]);
        return redirect('/userrole');
    }

    function destroy($id){
        //$query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        //deleteing an existing model on key

        UserRole::destroy($id);

        return redirect('/userrole');
    }
}
