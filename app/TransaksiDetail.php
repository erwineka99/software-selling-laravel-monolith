<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiDetail extends Model
{
    protected $table = "transaksi_detail";

    protected $fillable = ['id_item','transaksi_id','qty','harga','subtotal'];
}
