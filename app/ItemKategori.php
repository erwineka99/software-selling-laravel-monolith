<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class ItemKategori extends Model
{
    protected $table = "item_kategori";

    protected $fillable = ['nama','is_deleted'];

    public function item()
    {
        return $this->hasMany('App\Models\Item');
    }
}
