@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Daftar Transaksi</h4>

                </div>
            <div> <a href="/transaksi" class="btn btn-primary">Tambah</a></div>
                <h6 class="card-subtitle">&nbsp;</h6>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                        <thead class="bg-info text-white">
                            <tr>
                                <th style="width: 10%">No</th>
                                <th style="width: 40%%">Waktu Transaksi</th>
                                <th style="width: 20%%">Nama Pelanggan</th>
                                <th style="width: 20%%">Nilai Transaksi</th>
                                <th style="width: 10%">Detail</th>

                            </tr>
                        </thead>
                        <tbody class="border boder-info">
                            @forelse ($transaksi as $key=>$value)
                            <tr>
                                <td>{{$key + 1}}</th>
                                <td>{{$value->created_at}}</td>
                                <td>{{$value->nama_pelanggan}}</td>
                                <td>{{$value->jumlah_total}}</td>
                                <td style="display:flex;">

                                    <a href="/transaksi/list/{{$value->id}}" class="btn waves-effect waves-light btn-info">Show</a>

                                </td>
                            </tr>
                        @empty
                            <tr colspan="3">
                                <td colspan="5 align="center">No data</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('#zero_config').DataTable();
</script>
@endpush
