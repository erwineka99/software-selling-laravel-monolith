<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/adminmart/assets/images/logo-icon-sikasir-1.png')}}">
    <title>Sistem Informasi Kasir</title>
    <!-- Custom CSS -->
    <link href="{{asset('/adminmart/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <link href="{{asset('/adminmart/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('/adminmart/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="{{asset('/adminmart/dist/css/style.min.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->


    <!-- Datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

<![endif]-->
@stack('css')
</head>

<body>


<div class="card-body">
                                <h2 class="card-title" align="center">Detail Transaksi</h2>
                                
                                <p align="center"> 
                                <!--<a href="/transaksi/list" class="btn waves-effect waves-light btn-rounded btn-primary">cetak</a>-->
                                
                                </p>

                                <script type="text/javascript"> 
                                    $(document).ready(function () {
                                        $('#table-datatables').DataTable({
                                            dom: 'Bfrtip',
                                            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
                                        });
                                    });
                                </script>
                                <table class="table">
                                <tr>
                                        <td colspan="2">trans. no: {{$transaksi->id}}</td>
                                        
                                        <td colspan="3" align="right">date : {{$transaksi->created_at}}</td>
                                </tr>
                                
                                </table>
                                
                                <div class="table-responsive">
                                <table class="table">

                                        
                                        <thead>
                                            <tr>
                                                <th scope="col">no</th>
                                                <th scope="col">Nama Barang</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Harga</th>
                                                <th scope="col">Sub total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($transaksi_detail as $key=>$value)
                                            <tr>
                                                <td>{{$i++}}</th>
                                                <td>{{$value->nama}}</td>
                                                <td>{{$value->qty}}</td>
                                                <td>{{$value->harga}}</td>
                                                <td>{{$value->subtotal}}</td>
                                            </tr>
                                        @empty
                                            <tr colspan="3">
                                                <td colspan="3" align="center">No data</td>
                                            </tr>
                                        @endforelse
                                        <tr colspan="3">
                                                <td colspan="4" >total</td>
                                                <td>{{$transaksi_sum}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>

    </body>

    <script src="{{asset('/adminmart/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('/adminmart/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('/adminmart/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="{{asset('/adminmart/dist/js/app-style-switcher.js')}}"></script>
    <script src="{{asset('/adminmart/dist/js/feather.min.js')}}"></script>
    <script src="{{asset('/adminmart/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('/adminmart/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('/adminmart/dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <script src="{{asset('/adminmart/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script src="{{asset('/adminmart/assets/extra-libs/c3/c3.min.js')}}"></script>
    <script src="{{asset('/adminmart/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{asset('/adminmart/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('/adminmart/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('/adminmart/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('/adminmart/dist/js/pages/dashboards/dashboard1.min.js')}}"></script>


    <!--Datatables-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    @stack('scripts')
</body>

</html>