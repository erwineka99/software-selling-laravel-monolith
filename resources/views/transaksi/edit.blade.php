@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Ubah User Role</h4>
                </div>
                <form action="{{route('transaksi.update',['transaksi'=>$transaksi->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input type="text" class="form-control" name="nama" value="{{$transaksi->nama}}" id="isi" placeholder="Masukkan Nama Pelanggan">
                        @error('title')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
