<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{url('/datatables/favicon.ico')}}">

    <title>Transaksi {{$transaksi->created_at}} - oleh {{$transaksi->nama_pelanggan}} - total {{$transaksi_sum}}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('/datatables/css/bootstrap.min.css')}}" rel="stylesheet">
    <style type="text/css">
        body {
          padding-top: 20px;
          padding-bottom: 20px;
        }
        .navbar {
          margin-bottom: 20px;
        }
    </style>
    <!-- end bootstrap -->

    <!-- Untuk datatables -->
    <link rel="stylesheet" type="text/css" href="{{url('/datatables/css/datatables.bootstrap.css')}}">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <!-- End datatables -->

  </head>

  <body>
    <div class="container">
      <nav class="navbar navbar-light bg-faded">
        <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header" aria-expanded="false" aria-label="Toggle navigation"></button>
        <div class="collapse navbar-toggleable-xs" id="navbar-header">
          <a class="navbar-brand" href="/transaksi/list/">back</a>
        </div>
      </nav>

      <div class="row">
        <div class="col-md-10 offset-md-1">
            <h6 align="center">Detail Transaksi</h6>
            <p align="center">nama pelanggan: {{$transaksi->nama_pelanggan}}</p>
            <table class="table">
                <tr>
                        <td colspan="2">trans. no: {{$transaksi->id}}</td>
                        
                        <td colspan="3" align="right">date : {{$transaksi->created_at}}</td>
                </tr>
                
            </table>
            <table class="table table-hover" id="table-transaksi">
                <thead>
                    <tr>
                        <th align="left">ID</th>
                        <th align="left">Nama</th>
                        <th align="left">Harga</th>
                        <th align="left">Quantity</th>
                        <th align="left">Subtotal</th>
                    </tr>
                </thead>
            </table>
        </div>
      </div>
    </div> 

    <script src="{{url('/datatables/js/jquery.min.js')}}" ></script>
    <!-- ini untuk bootstrap -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" ></script>
    <script src="{{url('datatables/js/bootstrap.min.js')}}"></script>
    <!-- end bootstrap -->

    <!-- ini untuk datatables -->
    <script src="{{url('/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <!-- Pastikan posisi dibawah import datatablesnya -->
    
    <script type="text/javascript">
    
    $(function() {
        var oTable = $('#table-transaksi').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'print'
            ],
            
            processing: false,
            serverSide: true,
            ajax: {
                url: '{{ url("/transaksi/list/{$transaksi->id}/cetak") }}'
            },
            
            columns: [
            {data: 'id',   name: 'id'},
            {data: 'nama',     name: 'nama'},
            {data: 'harga',   name: 'harga'},
            {data: 'qty', name: 'qty',  orderable: false},
            {data: 'subtotal',  name: 'subtotal',   orderable: false, searchable: false},
        ],
        }
        
        );
    });

   
  </script>
  <!-- end datatables -->
  </body>
</html>
