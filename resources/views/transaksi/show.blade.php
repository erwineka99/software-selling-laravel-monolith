@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Transaksi</h4>
                        
                    </div>
                    <div class="col-5 align-self-center">
                        <div class="customize-input float-right">
                        <a href="/transaksi/list" class="btn btn-secondary">Back</a>
                        <a href="/transaksi/list/{{$transaksi->id}}/detail" class="btn btn-primary">Cetak</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
    
</div>

<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"></h4>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID transaksi</th>
                                                <th scope="col">Nama Pelanggan</th>
                                                <th scope="col">Jumlah total</th>
                                                <th scope="col">Waktu Transaksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">{{$transaksi->id}}</th>
                                                <td>{{$transaksi->nama_pelanggan}}</td>
                                                <td>{{$transaksi->jumlah_total}}</td>
                                                <td>{{$transaksi->created_at}}</td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Detail Transaksi</h4>
                                
                                <div class="table-responsive">
                                <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                    
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <th scope="col">no</th>
                                                <th scope="col">Nama Barang</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Harga</th>
                                                <th scope="col">Sub total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($transaksi_detail as $key=>$value)
                                            <tr>
                                                <td>{{$i++}}</th>
                                                <td>{{$value->nama}}</td>
                                                <td>{{$value->qty}}</td>
                                                <td>{{$value->harga}}</td>
                                                <td>{{$value->subtotal}}</td>
                                            </tr>
                                        @empty
                                            <tr colspan="3">
                                                <td colspan="3" align="center">No data</td>
                                            </tr>
                                        @endforelse
                                        <tr colspan="3">
                                                <td colspan="4" >total</td>
                                                <td>{{$transaksi_sum}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>

                    
                    
                    
                </div>
</div>
@endsection

@push('scripts')

@endpush
