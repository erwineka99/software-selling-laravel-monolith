@extends('layouts.master')
​
​
@push('css')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endpush
​
@section('content')


    ​<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4>Tambah Item</h4>
                    </div>
        <section class="content" id="dw">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
​
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Item</label>
                                        <select name="product_id" onchange="select_val();" id="product_id" class="form-control" required width="100%">
                                            <option value="">Pilih</option>
                                            @foreach ($item as $items)
                                            <option value="{{ $items->id }}"> {{ $items->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Qty</label>
                                        <input type="number" name="qty" id="qty" value="1" min="1" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-sm" onclick="tambah_data();">
                                            <i class="fa fa-shopping-cart"></i> Ke Keranjang
                                        </button>
                                    </div>
                                </div>

                                <!-- MENAMPILKAN DETAIL PRODUCT -->
                                <div class="col-md-5">
                                    <h4>Detail Produk</h4>
                                    <div v-if="product.name">
                                        <table class="table table-stripped">
                                            <tr>
                                                <th>Kode</th>
                                                <td>:</td>
                                                <td id="kolom_data_id"></td>
                                            </tr>
                                            <tr>
                                                <th width="3%">Produk</th>
                                                <td width="2%">:</td>
                                                <td id="kolom_data_nama"></td>
                                            </tr>
                                            <tr>
                                                <th>Harga</th>
                                                <td>:</td>
                                                <td id="kolom_data_harga"> </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </section>
                </div>
            </div>
        </div>
    </div>
    <form action="{{route('store_transaksi')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4>Keranjang</h4>
                    </div>
                <div></div>

                    <h6 class="card-subtitle">&nbsp;</h6>
                    <div class="form-group">
                        <label for="">Nama Customer</label>
                        <input style="width: 30%" required type="text" name="nama_pelanggan" id="nama" value="" min="" class="form-control">
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead class="bg-info text-white">
                                <tr>
                                    <th style="width: 5%">ID Item</th>
                                    <th style="width: 45%">Nama</th>
                                    <th style="width: 20%">Harga</th>
                                    <th style="width: 5%">Qty</th>
                                    <th style="width: 20%">Subtotal</th>
                                    <th style="width: 5%">Action</th>
                                </tr>
                            </thead>
                            <tbody id="keranjang_body" class="border boder-info">
                                <tr>
                                    <td></th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <tfoot class="border boder-info">

                                <tr>
                                    <td colspan="4">TOTAL<input type="hidden" name="total_keranjang" id="total_keranjang_form"/></th>
                                    <td colspan="2" id="total_keranjang"></td>
                                </tr>
                            </tfoot>
                        </table>
                        <button type="submit" class="btn btn-primary">Check Out</button></form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
@endsection
​
@push('scripts')

    <script>
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }

        });

        function select_val(){
            var a=$('#product_id').val();
            if(a!=""){
                //var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                //create ajax load product
                $.ajax({
                      //create an ajax request to display.php

                     type: "POST",
                    url: "/transaksi",
                    dataType: "json",   //expect html to be returned
                    data:{id:a},
                    success: function(response){
                        $("#kolom_data_id").html(response.id);
                        $("#kolom_data_nama").html(response.nama);
                        $("#kolom_data_harga").html(response.harga);
                        $("#qty").val(1);
                    }
                });

            }else{
                alert("Pilihlah Item yang benar");
                $("#kolom_data_id").html("");
                $("#kolom_data_nama").html("");
                $("#kolom_data_harga").html("");
                $("#qty").val("");
            }
        }
        var i=0;
        var total_final=0;
        function tambah_data(){
            var id=$("#kolom_data_id").html();
            var nama=$("#kolom_data_nama").html();
            var harga=$("#kolom_data_harga").html();
            var qty=$("#qty").val();
            var subtotal=Number(harga)*Number(qty);
            var succes=false;
            if(id!=""){
                i=i+1;
                $("#keranjang_body").append(`<tr id="row_keranjang_${i}">
                                    <td>${id}<input type="hidden" name="id_item[]" value="${id}" /></th>
                                    <td>${nama}<input type="hidden" name="nama_item[]" value="${nama}" /></td>
                                    <td>Rp. ${harga}<input type="hidden" name="harga_item[]" value="${harga}" /></td>
                                    <td id="qty_${id}">${qty}<input type="hidden" name="qty[]" value="${qty}" /></td>
                                    <td id="subtotal_${id}" class="subtotal">Rp. ${subtotal}<input type="hidden" class="subtotal_form" id="subtotal_form_${i}" name="subtotal[]" value="${subtotal}" /></td>
                                    <td><a href="javascript:hapus_kolom(${i})">Hapus</a></td>
                                </tr>`);



                                total_final=Number(total_final)+subtotal;
                                $('#total_keranjang_form').val(total_final);
                                $("#total_keranjang").html("Rp. "+total_final);


            }else{
                alert("Pilihlah Item yang benar terlebih dahulu");
            }

            if(succes==true){

            }
        }
        function hapus_kolom(id){
            var subtotal=$('#subtotal_form_'+id).val();
            //alert(subtotal);
            total_final=Number(total_final)-Number(subtotal);
            $('#total_keranjang_form').val(total_final);
            $("#total_keranjang").html("Rp. "+total_final);
            $('#row_keranjang_'+id).remove();
        }

    </script>
@endpush
