@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Detail Item</h4>
                </div>
                <table>
                    <tr>
                        <td>Nama Item
                        </td>
                        <td>:
                        </td>
                        <td>{{$item->nama}}
                        </td>
                    </tr>
                    <tr>
                        <td>Harga
                        </td>
                        <td>:
                        </td>
                        <td>{{$item->harga}}
                        </td>
                    </tr>
                    <tr>
                        <td>Kategori Item
                        </td>
                        <td>:
                        </td>
                        <td>{{$item->item_kategori}}
                        </td>
                    </tr>
                    <tr>
                        <td>Dibuat Tanggal
                        </td>
                        <td>:
                        </td>
                        <td>{{$item->created_at}}
                        </td>
                    </tr>
                    <tr>
                        <td>Diubah Tanggal
                        </td>
                        <td>:
                        </td>
                        <td>{{$item->updated_at}}
                        </td>
                    </tr>
                </table>
                <br><br>
                <div> <a href="{{ route('item.index')}}" class="btn btn-primary">Back</a></div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
