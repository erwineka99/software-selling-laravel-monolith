@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Tambah Item</h4>
                </div>
                <form action="{{route('item.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input type="text" class="form-control" name="nama"   id="isi" placeholder="Masukkan Nama Item">
                        <label for="title">Harga</label>
                        <input type="number" class="form-control" name="harga" id="isi" placeholder="Masukkan Harga Item">
                        <label for="title">Kategori Item</label>
                        <select class="custom-select" id="item_kategori" name="item_kategori" id="item_kategori">
                            <option value=""> --Silahkan Pilih-- </option>
                             @foreach ($item_kategori as $value)
                                   <option value="{{ $value->id }}">{{ $value->nama }}</option>
                             @endforeach
                        </select>
                        @error('title')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
