@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Info</div>
               
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            
                        </div>
                        
                    @endif

                    
                    <div class="d-flex justify-content-between">
                       
                        <h5 class="text-success ">You are logged in!</h5>
                        <a href="/dashboard" class="btn waves-effect waves-light btn-info">Go to Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
