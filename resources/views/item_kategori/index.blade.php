@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Item Kategori</h4>

                </div>
            <div> <a href="{{ route('itemkategori.create')}}" class="btn btn-primary">Tambah</a></div>
                <h6 class="card-subtitle">&nbsp;</h6>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                        <thead class="bg-info text-white">
                            <tr>
                                <th style="width: 10%">No</th>
                                <th style="width: 70%%">Nama</th>
                                <th style="width: 20%">Action</th>

                            </tr>
                        </thead>
                        <tbody class="border boder-info">
                            @forelse ($item_kategori as $key=>$value)
                            <tr>
                                <td>{{$key + 1}}</th>
                                <td>{{$value->nama}}</td>
                                <td style="display:flex;">

                                    <a href="{{route('itemkategori.show',['itemkategori'=>$value->id])}}" class="btn waves-effect waves-light btn-info">Show</a>
                                    <a href="{{route('itemkategori.edit',['itemkategori'=>$value->id])}}" class="btn waves-effect waves-light btn-warning">Edit</a>
                                    <form action="{{route('itemkategori.destroy',['itemkategori'=>$value->id])}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn waves-effect waves-light btn-danger" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr colspan="3">
                                <td colspan="3" align="center">No data</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('#zero_config').DataTable();
</script>
@endpush
