@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Detail Item Kategori</h4>
                </div>
                <table>
                    <tr>
                        <td>Nama Item Kategori
                        </td>
                        <td>:
                        </td>
                        <td>{{$item_kategori->nama}}
                        </td>
                    </tr>
                    <tr>
                        <td>Dibuat Tanggal
                        </td>
                        <td>:
                        </td>
                        <td>{{$item_kategori->created_at}}
                        </td>
                    </tr>
                    <tr>
                        <td>Diubah Tanggal
                        </td>
                        <td>:
                        </td>
                        <td>{{$item_kategori->updated_at}}
                        </td>
                    </tr>
                </table>
                <br><br>
                <div> <a href="{{ route('itemkategori.index')}}" class="btn btn-primary">Back</a></div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
