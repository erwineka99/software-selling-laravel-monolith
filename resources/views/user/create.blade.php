@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Tambah Data User</h4>
                </div>
                <form action="{{route('user.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="title">Nama User</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama User">
                        <label for="title">Email User</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Masukkan Email User">
                        <label for="title">Password</label>
                        <input type="text" class="form-control" name="password" id="password" placeholder="Masukkan Password User">
                        <label for="title">Role User</label>
                        <select class="custom-select" id="role_id" name="role_id" >
                            <option value=""> --Silahkan Pilih-- </option>
                            @foreach ($user_role as $value)
                                  <option value="{{ $value->id }}">{{ $value->nama }}</option>
                            @endforeach
                       </select>
                        
                        @error('title')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
